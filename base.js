console.log("Bonjour, bienvenue dans la console.");

let stringVar = 'Bonjour';

let numberVar = 45;

let booleanVar = true;

let arrayVar = [1, 2, 3, 4];

// Les chaines de charactères
console.warn(" <------ Les chaines de charactères -------->");
console.log('Pour concaténer des chaines de charactères (les assembler) nous utiliserons le symbole "+" en javascript');
console.log('stringVar est de type ' + typeof stringVar);
console.log('toUpperCase retourne le string en majuscule :' + stringVar.toUpperCase());
console.log('toLowerCase retourne le string en minuscule :' + stringVar.toLowerCase());
console.log('length retourne la longueur de la chaine :' + stringVar.length);

// Les number
console.warn(" <------ Les numbers -------->");
console.log("Sur les nombres, les principales choses à faire sont les opérations mathématiques: +, - , /, *");
console.log("Imaginons que nous recevons un nombre sous forme d'un string (par exemple \"45\")");
let wrongType = "455";
console.log("le type de wrongType est : " + typeof wrongType + " et sa valeur est de " + wrongType);
let goodType = parseFloat(wrongType); // Pour parser vers un entier on peut utiliser parseInt()
console.log("le type de goodType est : " + typeof goodType + " et sa valeur est de " + goodType);

console.warn(" <------ Les booleans -------->");
console.log(" Ils représentent un switch, c'est soit \"vrai\" soit \"faux\", on s'en servira surtout pour les conditions ou des variables d'état");

console.warn(" <------ Les arrays (tableaux) -------->");
console.log("Les arrays représentent un ensemble de valeurs qui sont stockées dans des index (numériques). La table des index commence à 0");
console.log("Une bonne pratique pour les arrays est d'emplier des valeurs du même type");
let students = ["Arnaud", "Benjamin", "Yael", "Rebecca"];
console.log("La valeur de l'array students =  " + students);
console.log(students);
console.log("Pour ajouter des valeurs à la fin de mon tableau, je peux utiliser la methode push()");
students.push("Florian");
console.log(students);
console.log("Pour connaître la longueur de mon tableau j'utilise la propriété length");
console.log("La longueur de mon tableau students est de " + students.length);
console.log("Pour accéder à une valeur précise de mon tableau j'utilise son index entre crochets ([]), par exemple students[2]");
console.log("La valeur de l'index 2 de students = " + students[2]);

console.log(" <------ Boucler sur un tableau (allez bien voir le code source) -------->");
console.log(" Pour boucler dans notre tableau, nous avons besoin d'une variable qui va représenter la valeur de l'index. Elle doit toujours être entre 0 et la longueur du tableau -1");
console.log('-- while --');
let i = 0;
while (i < students.length) {
    console.log("[while] la valeur à l'index " + i + " = " + students[i]);
    i ++;
}

console.log('-- for --');
for(let i = 0; i < students.length; i ++) {
    console.log("[for] la valeur à l'index " + i + " = " + students[i]);
}



console.warn(" <------ Les functions -------->");
console.log("Les functions sont des séquences d'instructions auquel on donne un nom (allez voir dans le code source)");
console.log("Pour executer une function, il suffit d'écrire son nom");
showArrayInConsole();
showArrayInConsole();
showArrayInConsole();
showArrayInConsole();
showArrayInConsole();


function showArrayInConsole() {
    // Dans le corps de la function on met la séquence d'instructions
    console.log("Execution de la function showArrayInConsole");
    for(let i = 0; i < students.length; i ++) {
        console.log("[for] la valeur à l'index " + i + " = " + students[i]);
    }
}

console.log("Nous pouvons donner un ou des arguments à une fonction.");
console.log("Ces arguments agiront comme une petite variable interne à la function");

let vegetables = ["Patate", "Tomate", "Choux", "Poivron"];
showArrayInConsole2(vegetables);
showArrayInConsole2(students);
function showArrayInConsole2(arrayToDisplay) {
    // Dans le corps de la function on met la séquence d'instructions
    console.log("Execution de la function showArrayInConsole avec arguments");
    for(let i = 0; i < arrayToDisplay.length; i ++) {
        console.log("[for] la valeur à l'index " + i + " = " + arrayToDisplay[i]);
    }
}

console.warn(" <------ Les conditions -------->");
console.log("On teste une valeur et si le test est positif on execute du code (allez voir le code source)");
arrayIsGreaterThanZero(vegetables);
arrayIsGreaterThanZero([]);
arrayIsGreaterThanZero(null);
arrayIsGreaterThanZero(variableUndefined);

function arrayIsGreaterThanZero(arrayToTest) {
    // Le truc à bien comprendre c'est que le navigateur va TOUJOURS essayer de transformer la condition en boolean
    // if (arrayToTest.length > 0) {
    // if (arrayToTest.length) {
    if (arrayToTest) {
        console.log("la condition arrayToTest.length > 0 est true");
    } else {
        console.log("la condition arrayToTest.length > 0 est false");
    }
}
